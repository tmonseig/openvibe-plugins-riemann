#ifndef __OpenViBEPlugins_Riemann_Defines_H__
#define __OpenViBEPlugins_Riemann_Defines_H__

//Covariance Estimator List
#define OVP_TypeId_Estimator											OpenViBE::CIdentifier(0x5261636B, 0x45535449)
#define OVP_TypeId_Estimator_COV										OpenViBE::CIdentifier(0x5261636B, 0x45434F56)
#define OVP_TypeId_Estimator_SCM										OpenViBE::CIdentifier(0x5261636B, 0x4553434D)
#define OVP_TypeId_Estimator_LWF		 								OpenViBE::CIdentifier(0x5261636B, 0x454C5746)
#define OVP_TypeId_Estimator_OAS										OpenViBE::CIdentifier(0x5261636B, 0x45414F53)
#define OVP_TypeId_Estimator_MCD										OpenViBE::CIdentifier(0x5261636B, 0x454D4344)
#define OVP_TypeId_Estimator_COR										OpenViBE::CIdentifier(0x5261636B, 0x45434F52)
/*******************************************************************************/

// Metric List
#define OVP_TypeId_Metric												OpenViBE::CIdentifier(0x5261636B, 0x4D455452)
#define OVP_TypeId_Metric_Riemann										OpenViBE::CIdentifier(0x5261636B, 0x4D524945)
#define OVP_TypeId_Metric_Euclidian										OpenViBE::CIdentifier(0x5261636B, 0x4D455543)
#define OVP_TypeId_Metric_Log_Euclidian		 							OpenViBE::CIdentifier(0x5261636B, 0x4D4C4745)
#define OVP_TypeId_Metric_LogDet										OpenViBE::CIdentifier(0x5261636B, 0x4D4C4744)
#define OVP_TypeId_Metric_Kullback										OpenViBE::CIdentifier(0x5261636B, 0x4D4B554C)
#define OVP_TypeId_Metric_Harmonic										OpenViBE::CIdentifier(0x5261636B, 0x4D484152)
#define OVP_TypeId_Metric_Wasserstein									OpenViBE::CIdentifier(0x5261636B, 0x4D574153)
#define OVP_TypeId_Metric_Identity										OpenViBE::CIdentifier(0x5261636B, 0x4D494445)
/*******************************************************************************/

//Shortcut for the cout
#define cout_W this->getLogManager() << LogLevel_Warning
#define cout_I this->getLogManager() << LogLevel_Info
#define cout_E this->getLogManager() << LogLevel_Error
/*******************************************************************************/

#endif // __OpenViBEPlugins_Riemann_Defines_H__

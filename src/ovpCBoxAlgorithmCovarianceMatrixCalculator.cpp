﻿#include "ovpCBoxAlgorithmCovarianceMatrixCalculator.h"
#include "utils/utils_OVConvert.hpp"


using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace RiemannianGeometry;

bool CBoxAlgorithmCovarianceMatrixCalculator::initialize()
{
	m_I0_Signal.initialize(*this, 0);
	m_O0_Matrix.initialize(*this, 0);
	m_Matrix = new CMatrix();
	MatrixInit(*m_Matrix);
	m_O0_Matrix.getInputMatrix().setReferenceTarget(m_Matrix);

	//***** Settings *****
	const CIdentifier Est(uint64(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0)));
	m_Center = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

		 if (Est == OVP_TypeId_Estimator_COV) {	m_Est = Estimator_COV;	cout_I << "Covariance Estimator\n"; }
	else if (Est == OVP_TypeId_Estimator_SCM) {	m_Est = Estimator_SCM;	cout_I << "Sample Covariance Matrix (SCM) Estimator\n"; }
	else if (Est == OVP_TypeId_Estimator_LWF) {	m_Est = Estimator_LWF;	cout_I << "Ledoit and Wolf Estimator\n"; }
	else if (Est == OVP_TypeId_Estimator_OAS) {	m_Est = Estimator_OAS;	cout_I << "Oracle Approximating Shrinkage (AOS) Estimator\n"; }
	else if (Est == OVP_TypeId_Estimator_MCD) {	m_Est = Estimator_MCD;	cout_I << "Minimum Covariance Determinant (MCD) Estimator\n"; }
	else if (Est == OVP_TypeId_Estimator_COR) {	m_Est = Estimator_COR;	cout_I << "Pearson Correlation Estimator\n"; }
	else { OV_ERROR_UNLESS_KRF(false, "Incorrect Selected Estimator", OpenViBE::Kernel::ErrorType::BadSetting); }

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmCovarianceMatrixCalculator::uninitialize()
{
	delete m_Matrix;

	m_I0_Signal.uninitialize();
	m_O0_Matrix.uninitialize();

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmCovarianceMatrixCalculator::processInput(uint32 ui32InputIndex)
{
	// ready to process !
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
/*******************************************************************************/


bool CBoxAlgorithmCovarianceMatrixCalculator::process()
{
	// the dynamic box context describes the current state of the box inputs and outputs (i.e. the chunks)
	IBoxIO& BoxContext = this->getDynamicBoxContext();

	for (uint32 i = 0; i < BoxContext.getInputChunkCount(0); ++i) {
		m_I0_Signal.decode(i);													// Décode le morceau
		const IMatrix* I_matrix = m_I0_Signal.getOutputMatrix();				// Recuperation du signal
		const uint64_t T_Start  = BoxContext.getInputChunkStartTime(0, i),		// Time Code Debut de morceau
					   T_End    = BoxContext.getInputChunkEndTime(0, i);		// Time Code Fin de morceau
		const uint32_t N_Channels = I_matrix->getDimensionSize(0);

		if (I_matrix->getDimensionCount() != 2) {
			cout_E << "Signal d\'entree invalide\n";
			//this->getLogManager() << LogLevel_Error << "Invalid Input Signal\n";
		}
		else if (m_I0_Signal.isBufferReceived()) {								// Buffer reçu
			CovarianceMatrix(*I_matrix, *m_Matrix, m_Center, m_Est);			// Calcul de la matrice
			m_O0_Matrix.encodeBuffer();											// Encodage du Buffer
		}
		else if (m_I0_Signal.isHeaderReceived()) {								// Header reçu
			MatrixResize(*m_Matrix, N_Channels);								// Mise à jour de la taille
			m_O0_Matrix.encodeHeader();											// Encodage du Buffer
		}	
		else if (m_I0_Signal.isEndReceived()) { m_O0_Matrix.encodeEnd(); }		// Fin recu

		BoxContext.markOutputAsReadyToSend(0, T_Start, T_End);					// Rend la sortie disponible
	}
	return true;
}

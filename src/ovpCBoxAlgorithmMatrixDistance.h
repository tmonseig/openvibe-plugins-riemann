#ifndef __OpenViBEPlugins_BoxAlgorithm_MatrixDistance_H__
#define __OpenViBEPlugins_BoxAlgorithm_MatrixDistance_H__

//You may have to change this path to match your folder organisation

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_MatrixDistance (0x5789da1f, 0x4eec0c45)
#define OVP_ClassId_BoxAlgorithm_MatrixDistanceDesc (0xd6e28086, 0x8a640e17)
#define OV_AttributeId_Box_FlagIsUnstable OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)


namespace OpenViBEPlugins
{
	namespace RiemannianGeometry
	{
		/**
		 * \class CBoxAlgorithmMatrixDistance
		 * \author Thibaut Monseigne (Inria)
		 * \date Tue Oct 23 15:01:14 2018
		 * \brief The class CBoxAlgorithmMatrixDistance describes the box Matrix Distance.
		 *
		 */
		class CBoxAlgorithmMatrixDistance : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(OpenViBE::uint32 ui32InputIndex) override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_MatrixDistance);

		protected:
			// Input decoder:
			OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmMatrixDistance> m_I0_Matrix;
			OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmMatrixDistance> m_I1_Matrix;
			// Output decoder:
			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmMatrixDistance> m_O0_Matrix;
		};


		/**
		 * \class CBoxAlgorithmMatrixDistanceDesc
		 * \author Thibaut Monseigne (Inria)
		 * \date Tue Oct 23 15:01:14 2018
		 * \brief Descriptor of the box Matrix Distance.
		 *
		 */
		class CBoxAlgorithmMatrixDistanceDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Matrix Distance"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Thibaut Monseigne"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Compute the distance between two matrix"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Compute the distance between two matrix with the specific metric"); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Riemannian Geometry"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-go-forward"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_MatrixDistance; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmMatrixDistance; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Impute Matrix A",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addInput("Input Matrix B",OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addOutput("Output Distance",OV_TypeId_StreamedMatrix);

				rBoxAlgorithmPrototype.addSetting("bla",OV_TypeId_Boolean, "true");

				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_MatrixDistanceDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_MatrixDistance_H__

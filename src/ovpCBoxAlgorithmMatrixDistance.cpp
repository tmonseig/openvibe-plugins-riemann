#include "ovpCBoxAlgorithmMatrixDistance.h"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace RiemannianGeometry;

bool CBoxAlgorithmMatrixDistance::initialize(void)
{
	m_I0_Matrix.initialize(*this, 0);
	m_I1_Matrix.initialize(*this, 1);
	m_O0_Matrix.initialize(*this, 0);

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmMatrixDistance::uninitialize(void)
{
	m_I0_Matrix.uninitialize();
	m_I1_Matrix.uninitialize();
	m_O0_Matrix.uninitialize();

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmMatrixDistance::processInput(uint32 ui32InputIndex)
{
	// some pre-processing code if needed...

	// ready to process !
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

/*******************************************************************************/


bool CBoxAlgorithmMatrixDistance::process(void)
{
	const IBox& l_rStaticBoxContext = this->getStaticBoxContext();
	IBoxIO& l_rDynamicBoxContext = this->getDynamicBoxContext();

	return true;
}

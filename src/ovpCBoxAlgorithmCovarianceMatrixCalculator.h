#ifndef __OpenViBEPlugins_BoxAlgorithm_CovarianceMatrixCalculator_H__
#define __OpenViBEPlugins_BoxAlgorithm_CovarianceMatrixCalculator_H__

//You may have to change this path to match your folder organisation
#include "ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

# include "utils/utils_Basics.hpp"
#include "utils/utils_Covariance.hpp"

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculator (0x9a93af80, 0x6449c826)
#define OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculatorDesc (0x12fcd91f, 0xd1d8f678)
#define OV_AttributeId_Box_FlagIsUnstable OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)


namespace OpenViBEPlugins
{
	namespace RiemannianGeometry
	{
		/**
		 * \class CBoxAlgorithmCovarianceMatrixCalculator
		 * \author Thibaut Monseigne (Inria)
		 * \date Tue Oct 16 10:32:06 2018
		 * \brief The class CBoxAlgorithmCovarianceMatrixCalculator describes the box Covariance Matrix Calculator.
		 *
		 */
		class CBoxAlgorithmCovarianceMatrixCalculator : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(OpenViBE::uint32 ui32InputIndex) override;

			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculator);

		protected:
			//***** Codecs *****
			OpenViBEToolkit::TSignalDecoder<CBoxAlgorithmCovarianceMatrixCalculator> m_I0_Signal;			// Input decoder
			OpenViBEToolkit::TStreamedMatrixEncoder<CBoxAlgorithmCovarianceMatrixCalculator> m_O0_Matrix;	// Output decoder
			OpenViBE::IMatrix* m_Matrix = nullptr;															// Covariance Matrix
			bool test = false;
			//***** Settings *****
			EEstimator_Type m_Est = Estimator_COV;
			bool m_Center = true;
		};


		/**
		 * \class CBoxAlgorithmCovarianceMatrixCalculatorDesc
		 * \author Thibaut Monseigne (Inria)
		 * \date Tue Oct 16 10:32:06 2018
		 * \brief Descriptor of the box Covariance Matrix Calculator.
		 *
		 */
		class CBoxAlgorithmCovarianceMatrixCalculatorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Covariance Matrix Calculator"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Thibaut Monseigne"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Calculation of the covariance matrix of the input signal."); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Calculation of the covariance matrix of the input signal.\nReturns a square matrix of size NxN. Where N is the number of channels."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Riemannian Geometry"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-execute"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculator; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmCovarianceMatrixCalculator; }
			
			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input Signal",OV_TypeId_Signal);
				rBoxAlgorithmPrototype.addOutput("Output Matrix",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addSetting("Estimator", OVP_TypeId_Estimator, "Covariance");
				rBoxAlgorithmPrototype.addSetting("Center Datas", OV_TypeId_Boolean, "true");
				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_CovarianceMatrixCalculatorDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_CovarianceMatrixCalculator_H__

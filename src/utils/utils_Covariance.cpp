# include "utils_Covariance.hpp"
#include <algorithm>    // std::max
#include <iostream>

#include <cstdint>
#include "utils_Basics.hpp"


using namespace Eigen;
using namespace std;

//***********************************************************
//******************** COVARIANCES BASES ********************
//***********************************************************
double Variance(const RowVectorXd& x)
{
	const double mu = x.mean();
	return x.cwiseProduct(x).sum() / x.cols() - mu * mu;
}
//****************************************************************************************************

double Covariance(const RowVectorXd& x, const RowVectorXd& y)
{
	return (x.cwiseProduct(y).sum() - x.sum() * y.sum() / x.cols()) / x.cols();
}
//****************************************************************************************************


MatrixXd ShrunkCovariance(const MatrixXd& cov, const double shrinkage)
{
	const size_t N = cov.rows();
	const double coef = shrinkage * cov.trace() / N;

	MatrixXd Res = (1 - shrinkage) * cov;
	for (size_t i = 0; i < N; ++i) {
		Res(i, i) += coef;
	}
	return Res;
}
//****************************************************************************************************

MatrixXd CovarianceMatrix(const MatrixXd& x, const bool center, const EEstimator_Type estimator)
{
	MatrixXd(*Covariancefunction)(const MatrixXd&) = CovarianceMatrixCOV;
	switch (estimator) {
	case Estimator_COV: break;
	case Estimator_COR: Covariancefunction = CovarianceMatrixCOR;
		break;
	case Estimator_SCM: Covariancefunction = CovarianceMatrixSCM;
		break;
	case Estimator_LWF: Covariancefunction = CovarianceMatrixLWF;
		break;
	case Estimator_OAS: Covariancefunction = CovarianceMatrixOAS;
		break;
	case Estimator_MCD: //Covariancefunction = CovarianceMatrixMCD;			break;
	default: return MatrixXd::Identity(x.rows(), x.rows());
	}
	return Covariancefunction(center ? MatrixCenter(x) : x);
}
//****************************************************************************************************

//***********************************************************
//***********************************************************
//***********************************************************

//***********************************************************
//******************** COVARIANCES TYPES ********************
//***********************************************************
MatrixXd CovarianceMatrixCOV(const MatrixXd& x)
{
	const size_t N = x.rows(),
		S = x.cols();

	MatrixXd Res(N, N);

	for (uint32_t i = 0; i < N; ++i) {
		const RowVectorXd ri = x.row(i);
		Res(i, i) = Variance(ri);

		for (uint32_t j = i + 1; j < N; ++j) {
			const RowVectorXd rj = x.row(j);
			Res(i, j) = Res(j, i) = Covariance(ri, rj);
		}
	}
	return Res;
}
//****************************************************************************************************


MatrixXd CovarianceMatrixSCM(const MatrixXd& x)
{
	const MatrixXd XXT = x * x.transpose();
	return XXT / XXT.trace();
}
//****************************************************************************************************

MatrixXd CovarianceMatrixLWF(const MatrixXd& x)
{
	const size_t N = x.rows(),				// Number of Features				=> N
		S = x.cols();						// Number of Samples				=> S
	const MatrixXd X2 = x.cwiseProduct(x),	// Squared each sample				=> X^2
		Cov = CovarianceMatrixCOV(x),		// Covariance Matrix				=> Cov
		Cov2 = Cov.cwiseProduct(Cov);		// Squared each element of Cov		=> Cov^2

	const double mu = Cov.trace() / N;
	MatrixXd Delta_ = Cov;
	for (size_t i = 0; i < N; ++i) {
		Delta_(i, i) -= mu;
	}
	const double Delta = (Delta_ * Delta_).sum() / N,
		Beta_ = 1. / (N * S) * (X2 * X2.transpose() / S - Cov2).sum(),
		Beta = min(Beta_, Delta),
		shrinkage = Beta / Delta;

	//cout << "Shrinkage : " << shrinkage << endl;
	return ShrunkCovariance(Cov, shrinkage);
}
//****************************************************************************************************

MatrixXd CovarianceMatrixOAS(const MatrixXd& x)
{
	const size_t N = x.rows(),
		S = x.cols();

	const MatrixXd Cov = CovarianceMatrixCOV(x);

	// Compute Shrinkage : Formula from Chen et al.'s
	const double mu = Cov.trace() / N,
		mu2 = mu * mu,
		alpha = (Cov * Cov).mean(),
		num = alpha + mu2,
		den = (S + 1) * (alpha - mu2 / N),
		shrinkage = den == 0 ? 1.0 : min(num / den, 1.0);
	//cout << "Shrinkage : " << shrinkage << endl;
	return ShrunkCovariance(Cov, shrinkage);
}
//****************************************************************************************************

MatrixXd CovarianceMatrixMCD(const MatrixXd& x)
{
	return MatrixXd::Identity(x.rows(), x.rows());
}
//****************************************************************************************************

MatrixXd CovarianceMatrixCOR(const MatrixXd& x)
{
	MatrixXd Cov = CovarianceMatrixCOV(x);
	MatrixXd d = Cov.diagonal().cwiseSqrt();

	for (size_t i = 0, N = Cov.rows(); i < N; ++i) {
		for (size_t j = 0; j < N; ++j) {
			Cov(i, j) = Cov(i, j) / (d(i) * d(j));
		}
	}
	return Cov;
}
//****************************************************************************************************

//***********************************************************
//***********************************************************
//***********************************************************

# include "utils_Basics.hpp"

using namespace Eigen;
using namespace std;

//***********************************************
//******************** BASES ********************
//***********************************************
RowVectorXd VectorCenter(const RowVectorXd& v)
{
	const double mu = v.mean();
	RowVectorXd Res = v;
	for (size_t i = 0, s = v.size(); i < s; ++i) {
		Res(i) -= mu;
	}
	return Res;
}
//****************************************************************************************************


MatrixXd MatrixCenter(const MatrixXd& m)
{
	MatrixXd Res = m;
	for (size_t i = 0, r = m.rows(), c = m.cols(); i < r; ++i) {
		const double mu = m.row(i).mean();
		for (size_t j = 0; j < c; ++j) {
			Res(i, j) -= mu;
		}
	}
	return Res;
}
//****************************************************************************************************
//***********************************************
//***********************************************
//***********************************************

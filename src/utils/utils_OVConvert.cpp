#include "utils_OVConvert.hpp"
#include <system/ovCMemory.h>

using namespace std;
using namespace OpenViBE;
using namespace Kernel;

using namespace Eigen;


//*****************************************************
//******************** CONVERSIONS ********************
//*****************************************************
MatrixXd MatrixConvert(const IMatrix& m)
{
	MatrixXd Res(m.getDimensionSize(0), m.getDimensionSize(1));
	System::Memory::copy(Res.data(), m.getBuffer(), m.getBufferElementCount() * sizeof(float64));
	return Res;
}
//****************************************************************************************************

void MatrixConvert(const MatrixXd& in, IMatrix& out)
{
	MatrixInit(out, in.rows(), in.cols());
	System::Memory::copy(out.getBuffer(), in.data(), out.getBufferElementCount() * sizeof(float64));
}
//****************************************************************************************************

void CovarianceMatrixToFeaturevector(const IMatrix& m, IMatrix& v)
{
	const uint32 N = m.getDimensionSize(0);
	const float64* M = m.getBuffer();
	float64* V = v.getBuffer();

	uint32 v_idx = 0;
	for (uint32 i = 0; i < N; ++i) {
		uint32 m_idx = i * (N + 1);
		V[v_idx++] = M[m_idx++];
		for (uint32 j = i + 1; j < N; ++j) {
			V[v_idx++] = M[m_idx++];
		}
	}
}
//****************************************************************************************************
//*****************************************************
//*****************************************************
//*****************************************************

//******************************************************************
//******************** LINKS TO EIGEN FUNCTIONS ********************
//******************************************************************
void CovarianceMatrix(const IMatrix& samples, IMatrix& cov, const bool center, const EEstimator_Type estimator)
{
	const MatrixXd S = MatrixConvert(samples),
	Cov = CovarianceMatrix(S, center, estimator);
	MatrixConvert(Cov, cov);
}
//****************************************************************************************************
//******************************************************************
//******************************************************************
//******************************************************************


//***********************************************************
//******************** MATRIX MANAGEMENT ********************
//***********************************************************
void MatrixInit(IMatrix& m, const uint32 rows, uint32 columns)
{
	if (columns < 1) { columns = rows; }
	MatrixResize(m, rows, columns);
	MatrixClean(m);
}
//****************************************************************************************************

void MatrixResize(IMatrix& m, const uint32 rows, uint32 columns)
{
	if (columns < 1) { columns = rows; }
	if (m.getDimensionCount() != 2 || m.getDimensionSize(0) != rows || m.getDimensionSize(1) != columns) {
		m.setDimensionCount(2);
		m.setDimensionSize(0, rows);
		m.setDimensionSize(1, columns);

		char str[10];
		for (uint32 i = 0; i < rows; ++i) {
			sprintf(str, "%u", i + 1);
			m.setDimensionLabel(0, i, str);
		}
		/* Labels sur les colonnes (inutile par d�faut de 1 � N)
		for (uint32 i = 0; i < columns; ++i) {
			sprintf(str, "%u", i + 1);
			m->setDimensionLabel(1, i, str);
		}
		*/
	}
}
//****************************************************************************************************

void MatrixClean(IMatrix& m)
{
	const uint32 size = m.getBufferElementCount();
	float64* Buffer = m.getBuffer();
	for (uint32 i = 0; i < size; ++i) { Buffer[i] = 0.0; }
}
//****************************************************************************************************
//***********************************************************
//***********************************************************
//***********************************************************

//***********************************************************
//******************** VECTOR MANAGEMENT ********************
//***********************************************************
void VectorInit(IMatrix& m, const uint32 N)
{
	VectorResize(m, N);
	MatrixClean(m);
}
//****************************************************************************************************

void VectorResize(IMatrix& m, const uint32 N)
{
	if (m.getDimensionCount() != 1 || m.getDimensionSize(0) != N) {
		m.setDimensionCount(1);
		m.setDimensionSize(0, N);
	}
}
//****************************************************************************************************
//***********************************************************
//***********************************************************
//***********************************************************

#ifndef __OpenViBEPlugins_Riemann_Utils_Basics_HPP__
#define __OpenViBEPlugins_Riemann_Utils_Basics_HPP__

#include <Eigen/Dense>


//***********************************************
//******************** BASES ********************
//***********************************************
///----------------------------------------------------------------------------------------------------
/// <summary>Removes the mean of the vector to this one</summary>
/// <param name="v">The Vector.</param>
/// <returns>Eigen RowVector : The centered Vector</returns>
Eigen::RowVectorXd VectorCenter(const Eigen::RowVectorXd& v);

///----------------------------------------------------------------------------------------------------
/// <summary>Removes the mean of each row at the matrix.</summary>
/// <param name="m">The Matrix.</param>
/// <returns>Eigen Matrix : The centered Matrix</returns>
Eigen::MatrixXd MatrixCenter(const Eigen::MatrixXd& m);

//***********************************************
//***********************************************
//***********************************************

#endif // __OpenViBEPlugins_Riemann_Utils_Basics_HPP__

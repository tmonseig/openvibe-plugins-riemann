#ifndef __OpenViBEPlugins_Riemann_Utils_Convert_HPP__
#define __OpenViBEPlugins_Riemann_Utils_Convert_HPP__

#include <openvibe/ov_all.h>
#include "utils_Covariance.hpp"

#include <Eigen/Dense>


//*****************************************************
//******************** CONVERSIONS ********************
//*****************************************************
///----------------------------------------------------------------------------------------------------
/// <summary>Convert OpenViBE Matrix to Eigen Matrix.</summary>
/// <param name="m">The OpenViBE::IMatrix.</param>
/// <returns>Eigen Matrix</returns>
Eigen::MatrixXd MatrixConvert(const OpenViBE::IMatrix& m);

///----------------------------------------------------------------------------------------------------
/// <summary>Convert Eigen Matrix to OpenViBE Matrix.</summary>
/// <param name="in">The Eigen::Matrix.</param>
/// <param name="out">The OpenViBE::IMatrix.</param>
void MatrixConvert(const Eigen::MatrixXd& in, OpenViBE::IMatrix& out);

///----------------------------------------------------------------------------------------------------
/// <summary>Convert a NxN Covariance Matrix to a N(N+1)/2 vector.</summary>
/// <param name="m">The Covariance Matrix.</param>
/// <param name="v">The Feature Vector.</param>
void CovarianceMatrixToFeaturevector(const OpenViBE::IMatrix& m, OpenViBE::IMatrix& v);
//*****************************************************
//*****************************************************
//*****************************************************

//******************************************************************
//******************** LINKS TO EIGEN FUNCTIONS ********************
//******************************************************************
///----------------------------------------------------------------------------------------------------
/// <summary>Select the function to call for the covariance matrix.\n
///			 centralizing the data is useless for <see cref="Estimator_COV"/> and <see cref="Estimator_COR"/>
/// </summary>
/// <param name="samples">The data set \f$x\f$. With \f$ N \f$ Rows (features) and \f$ S \f$ columns (samples) </param>
/// <param name="cov">The Covariance matrix.</param>
/// <param name="center">Centered the datas (each row is centered separately).</param>
/// <param name="estimator">The selected estimator (<see cref="EEstimator_Type"/>).</param>
void CovarianceMatrix(const OpenViBE::IMatrix& samples, OpenViBE::IMatrix& cov, const bool center = true, const EEstimator_Type estimator = Estimator_COV);
//******************************************************************
//******************************************************************
//******************************************************************

//***********************************************************
//******************** MATRIX MANAGEMENT ********************
//***********************************************************
///----------------------------------------------------------------------------------------------------
/// <summary>Initialize the matrix (do not create objects).</summary>
/// <param name="m">The matrix to initialize.</param>
/// <param name="rows">The number of rows.</param>
/// <param name="columns">The number of columns (if &lt; 1 Init to a Square Matrix) .</param>
void MatrixInit(OpenViBE::IMatrix& m, OpenViBE::uint32 rows = 2, OpenViBE::uint32 columns = 0);

///----------------------------------------------------------------------------------------------------
/// <summary>Resize the matrix (do not create objects).</summary>
/// <param name="m">The matrix to resize.</param>
/// <param name="rows">The number of rows.</param>
/// <param name="columns">The number of columns (if &lt; 1 resize to a Square Matrix) .</param>
void MatrixResize(OpenViBE::IMatrix& m, OpenViBE::uint32 rows = 2, OpenViBE::uint32 columns = 0);

///----------------------------------------------------------------------------------------------------
/// <summary>Set all value to Zeros.</summary>
/// <param name="m">The matrix to clean.</param>
void MatrixClean(OpenViBE::IMatrix& m);
//***********************************************************
//***********************************************************
//***********************************************************

//***********************************************************
//******************** VECTOR MANAGEMENT ********************
//***********************************************************
///----------------------------------------------------------------------------------------------------
/// <summary>Initialize the vector (matrix with one dimension) (do not create objects).</summary>
/// <param name="m">The vector to initialize.</param>
/// <param name="N">The number of elements.</param>
void VectorInit(OpenViBE::IMatrix& m, OpenViBE::uint32 N = 2);

///----------------------------------------------------------------------------------------------------
/// <summary>Resize the vector (matrix with one dimension) (do not create objects).</summary>
/// <param name="m">The vector to resize.</param>
/// <param name="N">The number of elements.</param>
void VectorResize(OpenViBE::IMatrix& m, OpenViBE::uint32 N = 2);
//***********************************************************
//***********************************************************
//***********************************************************

#endif // __OpenViBEPlugins_Defines_H__

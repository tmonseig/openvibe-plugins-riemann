#ifndef __OpenViBEPlugins_BoxAlgorithm_CovarianceMatrixToFeatureVector_H__
#define __OpenViBEPlugins_BoxAlgorithm_CovarianceMatrixToFeatureVector_H__

//You may have to change this path to match your folder organisation

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.
#define OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVector (0x7c265dba, 0x202c1f70)
#define OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVectorDesc (0xc0fb0445, 0x0d1cd546)
#define OV_AttributeId_Box_FlagIsUnstable OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)


namespace OpenViBEPlugins
{
	namespace RiemannianGeometry
	{
		/**
		 * \class CBoxAlgorithmCovarianceMatrixToFeatureVector
		 * \author Thibaut Monseigne (Inria)
		 * \date Wed Oct 17 09:52:38 2018
		 * \brief The class CBoxAlgorithmCovarianceMatrixToFeatureVector describes the box Covariance Matrix To Feature Vector.
		 *
		 */
		class CBoxAlgorithmCovarianceMatrixToFeatureVector : virtual public OpenViBEToolkit::TBoxAlgorithm<OpenViBE::Plugins::IBoxAlgorithm>
		{
		public:
			void release() override { delete this; }

			bool initialize() override;
			bool uninitialize() override;

			bool processInput(OpenViBE::uint32 ui32InputIndex) override;
			bool process() override;

			_IsDerivedFromClass_Final_(OpenViBEToolkit::TBoxAlgorithm < OpenViBE::Plugins::IBoxAlgorithm >, OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVector);

		protected:
			//***** Codecs *****
			OpenViBEToolkit::TStreamedMatrixDecoder<CBoxAlgorithmCovarianceMatrixToFeatureVector> m_I0_Matrix;		// Input decoder:
			OpenViBEToolkit::TFeatureVectorEncoder<CBoxAlgorithmCovarianceMatrixToFeatureVector> m_O0_Feature;		// Output decoder:
			OpenViBE::IMatrix* m_Matrix = nullptr;																	// Feature Matrix
		};


		/**
		 * \class CBoxAlgorithmCovarianceMatrixToFeatureVectorDesc
		 * \author Thibaut Monseigne (Inria)
		 * \date Wed Oct 17 09:52:38 2018
		 * \brief Descriptor of the box Covariance Matrix To Feature Vector.
		 *
		 */
		class CBoxAlgorithmCovarianceMatrixToFeatureVectorDesc : virtual public OpenViBE::Plugins::IBoxAlgorithmDesc
		{
		public:

			void release() override { }

			OpenViBE::CString getName() const override { return OpenViBE::CString("Covariance Matrix To Feature Vector"); }
			OpenViBE::CString getAuthorName() const override { return OpenViBE::CString("Thibaut Monseigne"); }
			OpenViBE::CString getAuthorCompanyName() const override { return OpenViBE::CString("Inria"); }
			OpenViBE::CString getShortDescription() const override { return OpenViBE::CString("Transforms a covariance matrix into a feature vector"); }
			OpenViBE::CString getDetailedDescription() const override { return OpenViBE::CString("Transforms a covariance matrix (size : NxN) into a feature vector (size : N(N+1)/2)."); }
			OpenViBE::CString getCategory() const override { return OpenViBE::CString("Riemannian Geometry"); }
			OpenViBE::CString getVersion() const override { return OpenViBE::CString("0.1"); }
			OpenViBE::CString getStockItemName() const override { return OpenViBE::CString("gtk-jump-to"); }

			OpenViBE::CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVector; }
			OpenViBE::Plugins::IPluginObject* create() override { return new CBoxAlgorithmCovarianceMatrixToFeatureVector; }

			bool getBoxPrototype(OpenViBE::Kernel::IBoxProto& rBoxAlgorithmPrototype) const override
			{
				rBoxAlgorithmPrototype.addInput("Input Covariance Matrix",OV_TypeId_StreamedMatrix);
				rBoxAlgorithmPrototype.addOutput("Output Feature Vector",OV_TypeId_FeatureVector);
				rBoxAlgorithmPrototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
				return true;
			}

			_IsDerivedFromClass_Final_(OpenViBE::Plugins::IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVectorDesc);
		};
	};
};

#endif // __OpenViBEPlugins_BoxAlgorithm_CovarianceMatrixToFeatureVector_H__

#include <openvibe/ov_all.h>
#include "ovp_defines.h"

// Boxes Includes
#include "ovpCBoxAlgorithmCovarianceMatrixCalculator.h"
#include "ovpCBoxAlgorithmCovarianceMatrixToFeatureVector.h"

OVP_Declare_Begin();

	// Register boxes
OVP_Declare_New(OpenViBEPlugins::RiemannianGeometry::CBoxAlgorithmCovarianceMatrixCalculatorDesc);
OVP_Declare_New(OpenViBEPlugins::RiemannianGeometry::CBoxAlgorithmCovarianceMatrixToFeatureVectorDesc);

	// Enumeration Estimator
	rPluginModuleContext.getTypeManager().registerEnumerationType(OVP_TypeId_Estimator, "Estimator");
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, "Covariance", OVP_TypeId_Estimator_COV.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, "Sample Covariance Matrix (SCM)", OVP_TypeId_Estimator_SCM.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, "Ledoit and Wolf", OVP_TypeId_Estimator_LWF.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, "Oracle Approximating Shrinkage (OAS)", OVP_TypeId_Estimator_OAS.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, "Minimum Covariance Determinant (MCD)", OVP_TypeId_Estimator_MCD.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Estimator, "Pearson Correlation", OVP_TypeId_Estimator_COR.toUInteger());

	// Enumeration Metric
	rPluginModuleContext.getTypeManager().registerEnumerationType(OVP_TypeId_Metric, "Metric");
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, "Riemann", OVP_TypeId_Metric_Riemann.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, "Euclidian", OVP_TypeId_Metric_Euclidian.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, "Log-Euclidian", OVP_TypeId_Metric_Log_Euclidian.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, "Kullback", OVP_TypeId_Metric_Kullback.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, "Harmonic", OVP_TypeId_Metric_Harmonic.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, "Wasserstein", OVP_TypeId_Metric_Wasserstein.toUInteger());
	rPluginModuleContext.getTypeManager().registerEnumerationEntry(OVP_TypeId_Metric, "Identity", OVP_TypeId_Metric_Identity.toUInteger());

OVP_Declare_End();

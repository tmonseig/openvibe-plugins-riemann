#include "ovpCBoxAlgorithmCovarianceMatrixToFeatureVector.h"

#include "ovp_defines.h"
#include "utils/utils_OVConvert.hpp"

using namespace OpenViBE;
using namespace Kernel;
using namespace Plugins;

using namespace OpenViBEPlugins;
using namespace RiemannianGeometry;

bool CBoxAlgorithmCovarianceMatrixToFeatureVector::initialize()
{
	m_I0_Matrix.initialize(*this, 0);
	m_O0_Feature.initialize(*this, 0);
	m_Matrix = new CMatrix();
	VectorInit(*m_Matrix);
	m_O0_Feature.getInputMatrix().setReferenceTarget(m_Matrix);

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmCovarianceMatrixToFeatureVector::uninitialize()
{
	m_I0_Matrix.uninitialize();
	m_O0_Feature.uninitialize();

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmCovarianceMatrixToFeatureVector::processInput(uint32 ui32InputIndex)
{
	// ready to process !
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmCovarianceMatrixToFeatureVector::process()
{
	IBoxIO& BoxContext = this->getDynamicBoxContext();
	for (uint32 i = 0; i < BoxContext.getInputChunkCount(0); ++i) {
		m_I0_Matrix.decode(i);													// D�code le morceau
		const IMatrix* I_matrix = m_I0_Matrix.getOutputMatrix();				// Recuperation du signal
		const uint64_t T_Start = BoxContext.getInputChunkStartTime(0, i),		// Time Code Debut de morceau
					   T_End = BoxContext.getInputChunkEndTime(0, i);			// Time Code Fin de morceau
		const uint32_t N_Channels = I_matrix->getDimensionSize(0);

		if (I_matrix->getDimensionCount() != 2) {
			cout_E << "Signal d\'entree invalide\n";
			//this->getLogManager() << LogLevel_Error << "Invalid Input Signal\n";
		}
		else if (m_I0_Matrix.isBufferReceived()) {								// Buffer re�u
			CovarianceMatrixToFeaturevector(*I_matrix, *m_Matrix);				// Transformation
			m_O0_Feature.encodeBuffer();										// Encodage du Buffer
		}
		else if (m_I0_Matrix.isHeaderReceived()) {								// Header re�u
			VectorResize(*m_Matrix, N_Channels*(N_Channels+1)/2);				// Mise � jour de la taille
			m_O0_Feature.encodeHeader();										// Encodage du Buffer
		}
		else if (m_I0_Matrix.isEndReceived()) { m_O0_Feature.encodeEnd(); }		// Fin recu

		BoxContext.markOutputAsReadyToSend(0, T_Start, T_End);					// Rend la sortie disponible
	}
	return true;
}
